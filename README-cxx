================================================================================
About the cxx interface
================================================================================

The cxx interface consists of a single namespace named 'SDLU', which in turn
provides the following classes:

SDLU::Renderer      -> SDL_Renderer
SDLU::RWops         -> SDL_RWops
SDLU::Texture       -> SDL_Texture
SDLU::Window        -> SDL_Window
SDLU::Button        -> SDLU_Button
SDLU::IniHandler    -> SDLU_IniHandler
SDLU::Sprite        -> SDLU_Sprite
SDLU::Turtle        -> SDLU_Turtle

All classes are named after the original structure they wrap. For the rest of
this document, we will refer to them as 'SDLU classes'.

Note that the cxx interface does not affect any C code, neither does it disable
the SDL2 C API. All of your existing code will work as it normally would, with
or without the cxx interface.


================================================================================
Building the cxx interface
================================================================================

The cxx interface is an optional feature of SDLU, since it requires a working
C++ compiler. For that reason, it has to be explicitly enabled during the
generation of the project files.

    $ cmake -DBUILD_CXX=ON [other_options]
    $ ./configure --enable-cxx [other_options]

For more information on building SDLU, see INSTALL, README-configure as well as
README-cmake.


================================================================================
Checking for the cxx interface
================================================================================

If you want to check in your program whether you have the cxx interface, you can
do so by checking if the macro 'SDLU_CXX' is defined:

    #if defined(SDLU_CXX)
        /* We have the cxx interface */
    #else
        /* Whoops, no cxx interface */
    #endif


================================================================================
Anatomy of the cxx interface
================================================================================

Internally, the SDLU classes keep a structure of the associated type. So,
SDLU::Window will keep internally an SDL_Window structure. The original
structures can be acquired from an SDLU Class with the method 'Get<structure>'.

There are no 'create' methods in SDLU classes, the job of creation and deletion
is handed to the appropriate constructors and destructors. The constructors and
the destructors are overloaded, so that they cover all possible 'SDL_Create()'
SDL2 calls. Also, there is one extra constructor that makes an SDLU class
object from the original structure.

Additionally, all methods are overloaded so that when they need a window, a
renderer a texture, a sprite, or a button as a parameter, you can pass both an
SDLU class object and a structure of the same type. The exception is some
methods like 'SDLU::Renderer::GetRenderTarget()', which cannot be overloaded.
[C++ does not allow overloading functions or methods by just changing the type
of the return value].

If you need help, you can always browse 'include/SDLU_cxx.h' to see the class
definitions. This will clear things up, and should help you understand the
internal structure of the cxx interface.

Other than that, all the methods of an SDLU class are no different than the ones
of the SDL2 API. The naming scheme of all methods is this:

    SDL_[get/set]{object}{property} ----> {object}->[get/set]{property}
    SDL_{object}{action}            ----> {object}->{action}

For a complete example, you might want to check out 'testcxx' from the "test/"
subdirectory.

The two pieces of code above do exactly the same thing. The second one is using
the cxx interface:

/*******************************************************
 *
 * This code uses the SDL2 C API
 *
 *******************************************************/

    SDL_Window* window;
    SDL_Renderer* renderer;

    SDL_CreateWindow("title", x, y, w, h, window_flags);
    SDL_CreateRenderer(window, driver, renderer_flags);

    SDL_SetWindowTitle(window, "new title");

    SDL_RenderClear(renderer);
    SDL_RenderDrawLine(renderer, 100, 100, 200, 200);
    SDL_RenderPresent(renderer);

    SDL_SetWindowTitle(window, "final title");

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

/*******************************************************
 *
 * This code uses the cxx interface
 *
 *******************************************************/

    SDLU::Window *window;
    SDLU::Renderer *renderer;

    window = new SDLU::Window("title", x, y, w, h, window_flags);
    renderer = new SDLU::Renderer(window, driver, renderer_flags);

    window->SetTitle("new title");

    renderer->Clear();
    renderer->DrawLine(100, 100, 200, 200);
    renderer->Present();

    destroy renderer, window;


-- Aggelos Kolaitis
